json.extract! mainprogram, :id, :created_at, :updated_at
json.url mainprogram_url(mainprogram, format: :json)
