require 'test_helper'

class MainprogramsControllerTest < ActionController::TestCase
  setup do
    @mainprogram = mainprograms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mainprograms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mainprogram" do
    assert_difference('Mainprogram.count') do
      post :create, mainprogram: {  }
    end

    assert_redirected_to mainprogram_path(assigns(:mainprogram))
  end

  test "should show mainprogram" do
    get :show, id: @mainprogram
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mainprogram
    assert_response :success
  end

  test "should update mainprogram" do
    patch :update, id: @mainprogram, mainprogram: {  }
    assert_redirected_to mainprogram_path(assigns(:mainprogram))
  end

  test "should destroy mainprogram" do
    assert_difference('Mainprogram.count', -1) do
      delete :destroy, id: @mainprogram
    end

    assert_redirected_to mainprograms_path
  end
end
