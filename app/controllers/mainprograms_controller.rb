require 'rubygems'
require 'googlecharts'
require 'gchart'
require 'roo'
require 'roo-xls'

class MainprogramsController < ApplicationController
  before_action :set_mainprogram, only: [:show, :edit, :update, :destroy]

  $month = 'none'

  # GET /mainprograms
  # GET /mainprograms.json
  def index
    @mainprograms = Mainprogram.all
  end

  # GET /mainprograms/1
  # GET /mainprograms/1.json
  def show
  end
  
  def jan
   redirect_to mainpage_path
   $month = 'jan'
  end
  
  def feb
   redirect_to mainpage_path
   $month = 'feb'
  end
  
  def mainpage
    if $month != 'none'
      if $month == 'jan'
        @title = "January Delay Reason %"
        @exceltable = 'FlightJan.xls'
      end
      if $month == 'feb'
        @title = "Febuary Delay Reason %"
        @exceltable = 'FlightFeb.xls'
      end
      s = Roo::Excel.new(Rails.root.join('app', 'assets', @exceltable))
      s.default_sheet = s.sheets.first
      $i = 1
      $o = 1
      $k = 1
      $l = 1
      $m = 1
      $num = 21
      carrierd = 0
      weatherd = 0
      nasd = 0
      securityd = 0
      lateplaned = 0
      while $i < $num  do
        if s.cell('B',$i) != 'NA'
          carrierd = carrierd + s.cell('B',$i).to_i
        end
        $i +=1
      end
      while $o < $num  do
        if s.cell('C',$o) != 'NA'
          weatherd = weatherd + s.cell('C',$o).to_i
        end
        $o +=1
      end
      while $k < $num  do
        if s.cell('D',$k) != 'NA'
          nasd = nasd + s.cell('D',$k).to_i
        end
        $k +=1
      end
      while $l < $num  do
        if s.cell('E',$l) != 'NA'
          securityd = securityd + s.cell('E',$l).to_i
        end
        $l +=1
      end
      while $m < $num  do
        if s.cell('F',$m) != 'NA'
          lateplaned = lateplaned + s.cell('F',$m).to_i
        end
        $m +=1
      end
      @pie_chart = Gchart.pie_3d(:title => @title, :labels => ['Carrier', 'Weather', 'NAS', 'Security', 'Late Plane',], :data => [carrierd, weatherd, nasd, securityd, lateplaned], :size => '400x200')
    end
  end

  # GET /mainprograms/new
  def new
    @mainprogram = Mainprogram.new
  end

  # GET /mainprograms/1/edit
  def edit
  end

  # POST /mainprograms
  # POST /mainprograms.json
  def create
    @mainprogram = Mainprogram.new(mainprogram_params)

    respond_to do |format|
      if @mainprogram.save
        format.html { redirect_to @mainprogram, notice: 'Mainprogram was successfully created.' }
        format.json { render :show, status: :created, location: @mainprogram }
      else
        format.html { render :new }
        format.json { render json: @mainprogram.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mainprograms/1
  # PATCH/PUT /mainprograms/1.json
  def update
    respond_to do |format|
      if @mainprogram.update(mainprogram_params)
        format.html { redirect_to @mainprogram, notice: 'Mainprogram was successfully updated.' }
        format.json { render :show, status: :ok, location: @mainprogram }
      else
        format.html { render :edit }
        format.json { render json: @mainprogram.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mainprograms/1
  # DELETE /mainprograms/1.json
  def destroy
    @mainprogram.destroy
    respond_to do |format|
      format.html { redirect_to mainprograms_url, notice: 'Mainprogram was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mainprogram
      @mainprogram = Mainprogram.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mainprogram_params
      params.fetch(:mainprogram, {})
    end
end
