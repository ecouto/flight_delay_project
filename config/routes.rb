Rails.application.routes.draw do
  
root 'users#login'
get "/app/views/users/login.html.erb", to: "users#login", as: "login"
get "/app/views/mainprograms/mainpage.html.erb", to: "mainprograms#mainpage", as: "mainpage"
 
  resources :mainprograms do
  #get 'mainp/page'
    collection do
      post :mainpage
      post :jan
      post :feb
    end
  end
  
  resources :users do
    collection do
      post :logintoaccount
    end
  end
  
end